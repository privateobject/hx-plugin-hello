"use strict";
const hx = require("hbuilderx");
const i18 = require("../i18/_.js");
let btns = [i18('yes'),i18('no')];

function showInfoMsg(command = 'extension.info_msg', msg = '插件 消息弹窗') {
	return hx.commands.registerCommand(command, () => {
		console.log('->消息弹窗', command, msg);
		hx.window.showInformationMessage(msg, btns).then((result) => {
			console.log("->消息弹窗 选择",result);
		});
	});
}

function showErrMsg(command = 'extension.err_msg', msg = '插件 异常弹窗') {
	return hx.commands.registerCommand(command, () => {
		console.log('->异常弹窗', command, msg);
		hx.window.showErrorMessage(msg, btns).then((result) => {
			console.log("->异常弹窗 选择",result);
			hx.window.showInformationMessage(msg + ' ' + result);
		});
	});
}

function showWarnMsg(command = 'extension.warn_msg', msg = '插件 警告弹窗') {
	return hx.commands.registerCommand(command, () => {
		console.log('->警告弹窗', command, msg);
		hx.window.showWarningMessage(msg, btns).then((result) => {
			console.log("->警告弹窗 选择",result);
		});
	});
}

module.exports = {
	showInfoMsg,
	showErrMsg,
	showWarnMsg
}