var language;
function uiI18(){
	const HX = require("hbuilderx"), PATH = require("path"), FS = require("fs"), ln = HX.env.language;
	let fil = PATH.resolve(__dirname, "./_"+ln+".json");
	console.log('load-language', ln, __dirname);
	if(FS.existsSync(fil)){
		return JSON.parse(FS.readFileSync(fil, { encoding: "utf8" }));
	}
	return JSON.parse(FS.readFileSync(PATH.resolve(__dirname, "./_zh_CN.json"), { encoding: "utf8" }));
}
var get = function(key = 'def') {
	if(!language){ language = uiI18(); }
	return language[key] || key;
}
module.exports = get;