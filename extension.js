// var hx = require("hbuilderx");
const h_msg = require('./dialog/message.js');
const i18 = require('./i18/_.js');
//该方法将在插件激活的时候调用
function activate(context) {
	console.log('->插件激活', context)
	//订阅销毁钩子，插件禁用的时候，自动注销该command。
	context.subscriptions.push(h_msg.showInfoMsg('extension.hello_msg_inf', i18('hello_msg_inf')));
	context.subscriptions.push(h_msg.showErrMsg('extension.hello_msg_err', i18('hello_msg_err')));
	context.subscriptions.push(h_msg.showWarnMsg('extension.hello_msg_warn', i18('hello_msg_warn')));

}
//该方法将在插件禁用的时候调用（目前是在插件卸载的时候触发）
function deactivate() {
	console.log('->插件卸载')
}
module.exports = {
	activate,
	deactivate
}